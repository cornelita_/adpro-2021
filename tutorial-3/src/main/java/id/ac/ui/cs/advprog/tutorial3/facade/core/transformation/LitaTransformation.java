package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class LitaTransformation {
    private int key;

    public LitaTransformation() {
        this.key = 'L' + 'i' + 't' + 'a';
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int codexSize = codex.getCharSize();
        int selector = encode ? -1 : 1;
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            int charIdx = codex.getIndex(text.charAt(i));
            int newCharIdx = (charIdx + key * selector) % codexSize;
            newCharIdx = newCharIdx < 0 ? codexSize + newCharIdx : newCharIdx;
            res[i] = codex.getChar(newCharIdx);
        }
        return new Spell(new String(res), codex);
    }

    public Spell encodeCall(Spell spell){
        return CodexTranslator.translate((encode(spell)), RunicCodex.getInstance());
    }

    public Spell decodeCall(Spell spell){
        return (new AbyssalTransformation()).decodeCall(decode(spell));
    }
}
