package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class TransformationFacade {

    public static String encode(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        return (new CelestialTransformation()).encodeCall(spell).getText();
    }

    public static String decode(String code) {
        Spell spell = new Spell(code, RunicCodex.getInstance());
        return (new LitaTransformation()).decodeCall(spell).getText();
    }
}
