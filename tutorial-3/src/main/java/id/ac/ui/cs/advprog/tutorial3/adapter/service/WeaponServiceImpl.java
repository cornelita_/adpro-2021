package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;

//    private Map<String, Weapon> allWeapons;

    @PostConstruct
    private void adaptation() {
        for (Spellbook spell : spellbookRepository.findAll()) {
            weaponRepository.save(new SpellbookAdapter(spell));
        }
        for (Bow bow : bowRepository.findAll()) {
            weaponRepository.save((new BowAdapter(bow)));
        }
//        allWeapons = new HashMap<>();
//        for (Weapon weapon : weaponRepository.findAll()) allWeapons.put(weapon.getName(), weapon);
//        for (Spellbook spell : spellbookRepository.findAll()) allWeapons.put(spell.getName(), new SpellbookAdapter(spell));
//        for (Bow bow : bowRepository.findAll()) allWeapons.put(bow.getName(), new BowAdapter(bow));
    }

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        for (Spellbook spell : spellbookRepository.findAll()) {
            weaponRepository.save(new SpellbookAdapter(spell));
        }
        for (Bow bow : bowRepository.findAll()) {
            weaponRepository.save((new BowAdapter(bow)));
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if (attackType == 0) {
            logRepository.addLog(String.format("%s attacked with %s (normal attack): %s",weapon.getHolderName(), weaponName, weapon.normalAttack()));
        }
        else {
            logRepository.addLog(String.format("%s attacked with %s (charged attack): %s",weapon.getHolderName(), weaponName, weapon.chargedAttack()));
        }
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
