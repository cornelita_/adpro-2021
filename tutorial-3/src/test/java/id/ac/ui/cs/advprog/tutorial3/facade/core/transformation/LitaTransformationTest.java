package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LitaTransformationTest {
    private Class<?> litaClass;

    @BeforeEach
    public void setup() throws Exception {
        litaClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.LitaTransformation");
    }

    @Test
    public void testLitaHasEncodeMethod() throws Exception {
        Method translate = litaClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLitaEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "CKPSbKuKXNu3ugOXdudYuKuLVKMUcWSdRudYuPYbQOuYebucgYbN";

        Spell result = new LitaTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testLitaHasDecodeMethod() throws Exception {
        Method translate = litaClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLitaDecodesCorrectly() throws Exception {
        String text = "CKPSbKuKXNu3ugOXdudYuKuLVKMUcWSdRudYuPYbQOuYebucgYbN";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new LitaTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
