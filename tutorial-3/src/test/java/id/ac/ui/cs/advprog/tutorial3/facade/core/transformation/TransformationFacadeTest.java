package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransformationFacadeTest {
    private Class<?> transformationFacadeClass;
    private AbyssalTransformation abyssal;
    private CelestialTransformation celestial;
    private LitaTransformation lita;

    @BeforeEach
    public void setup() throws Exception {
        transformationFacadeClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.TransformationFacade");
        abyssal = new AbyssalTransformation();
        celestial = new CelestialTransformation();
        lita = new LitaTransformation();
    }

    @Test
    public void testTransformationFacadeHasEncodeMethod() throws Exception {
        Method translate = transformationFacadeClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationFacadeEncodesCorrectly() throws Exception {
        String text = "Semangattt buat siapapun yang baca inii";
        Codex runicCodex = RunicCodex.getInstance();
        Codex alphaCodex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, alphaCodex);
        Spell expected = CodexTranslator.translate(lita.encode(abyssal.encode(celestial.encode(spell))), runicCodex);

        String result = TransformationFacade.encode(text);
        assertEquals(expected.getText(), result);
    }

    @Test
    public void testTransformationFacadeHasDecodeMethod() throws Exception {
        Method translate = transformationFacadeClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationFacadeDecodesCorrectly() throws Exception {
        String text = ":vXyJ>-nzxt?BVb*qVnS:Vm=b>sFxGmoat*quaL";
        Codex runicCodex = RunicCodex.getInstance();
        Codex alphaCodex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, runicCodex);
        Spell expected = CodexTranslator.translate(celestial.decode(abyssal.decode(lita.decode(spell))), alphaCodex);

        String result = TransformationFacade.decode(text);
        assertEquals(expected.getText(), result);
    }
}
