package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_log")
    private int idLog;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    private LocalDateTime endTime;

    @Column(name = "deskripsi", columnDefinition = "TEXT")
    private String deskripsi;

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "npm", nullable = false)
    private Mahasiswa mahasiswa;

    public Log (LocalDateTime startTime, LocalDateTime endTime, String deskripsi, Mahasiswa mahasiswa) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.deskripsi = deskripsi;
        this.mahasiswa = mahasiswa;
    }
}
