package csui.advpro2021.tais.model;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "laporan")
@Data
@NoArgsConstructor
public class Laporan {
   private static final int GAJI = 350;
   private static String[] monthNames = {
           "January", "February", "March", "April", "May", "June",
           "July", "August", "September", "October", "November", "December"};

   @Id
   @Column(name = "month", updatable = false)
   private String month;

   @Column(name = "month_int", updatable = false)
   private int monthInt;

   @Column(name = "jam_kerja")
   private float jamKerja;

   @Column(name = "pembayaran")
   private long pembayaran;

   @NotNull
   @ManyToOne
   @JoinColumn(name = "mahasiswa", nullable = false)
   private Mahasiswa mahasiswa;

   public Laporan (int month, float jamKerja, Mahasiswa mahasiswa) {
       this.monthInt = month;
       this.month = monthNames[month];
       this.jamKerja = jamKerja;
       this.pembayaran = (long)(this.jamKerja * GAJI);
       this.mahasiswa = mahasiswa;
   }
}
