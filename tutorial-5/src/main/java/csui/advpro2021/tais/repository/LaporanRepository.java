package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LaporanRepository extends JpaRepository<Laporan, String> {
    List<Laporan> findLaporansByMahasiswa(Mahasiswa mahasiswa);
    Laporan findLaporanByMahasiswaAndMonthInt(Mahasiswa mahasiswa, int monthInt);
}
