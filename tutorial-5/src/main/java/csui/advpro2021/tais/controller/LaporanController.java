package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.service.LaporanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/laporan")
public class LaporanController {
    @Autowired
    private LaporanService laporanService;

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Laporan>> getLogsByMahasiswa(@PathVariable String npm) {
        return ResponseEntity.ok(laporanService.getListLaporanByMahasiswa(npm));
    }
}
