package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createLog(@RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(log));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getLogsByMahasiswa(@RequestBody Mahasiswa mahasiswa) {
        return ResponseEntity.ok(logService.getLogsByMahasiswa(mahasiswa));
    }

    @GetMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable int idLog) {
        Log log = logService.getLogById(idLog);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(log);
    }

    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable int idLog) {
        logService.deleteLogById(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(path = "/update", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(log));
    }
}
