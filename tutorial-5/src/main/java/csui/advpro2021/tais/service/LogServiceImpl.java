package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl implements LogService{
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Log createLog(Log log) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(log.getMahasiswa().getNpm());
        if (mahasiswa != null) {
            log.setMahasiswa(mahasiswa);
            return logRepository.save(log);
        }
        return log;
    }

    @Override
    public Iterable<Log> getLogsByMahasiswa(Mahasiswa mahasiswa) {
        return logRepository.findAllByMahasiswa(mahasiswa);
    }

    @Override
    public Log getLogById(int idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public void deleteLogById(int idLog) {
        logRepository.deleteById(idLog);
    }

    @Override
    public Log updateLog(Log log) {
        Log oldLog = logRepository.findByIdLog(log.getIdLog());
        oldLog.setStartTime(log.getStartTime());
        oldLog.setEndTime(log.getEndTime());
        oldLog.setDeskripsi(log.getDeskripsi());
        oldLog.setMahasiswa(log.getMahasiswa());
        logRepository.save(oldLog);
        return oldLog;
    }
}
