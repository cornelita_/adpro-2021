package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LaporanRepository;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Arrays;

@Service
public class LaporanServiceImpl implements LaporanService {
    @Autowired
    private LaporanRepository laporanRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private LogRepository logRepository;

    @Override
    public Mahasiswa createLaporan(Laporan laporan) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(laporan.getMahasiswa().getNpm());
        if (mahasiswa != null) {
            laporan.setMahasiswa(mahasiswa);
            laporanRepository.save(laporan);
            return mahasiswaRepository.findByNpm(laporan.getMahasiswa().getNpm());
        }
        return mahasiswa;
    }

    @Override
    public Iterable<Laporan> getListLaporanByMahasiswa(String npm) {
       Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
       if (mahasiswa != null) {
           return updateLaporan(mahasiswa);
       }
       return null;
    }

    @Override
    public Iterable<Laporan> updateLaporan(Mahasiswa mahasiswa) {
       Iterable<Log> logMahasiswa = logRepository.findAllByMahasiswa(mahasiswa);
       Float[] tempJamKerja = new Float[12];
       for (int i = 0; i < 12; i++) tempJamKerja[i] = (float)0;

       for (Log log : logMahasiswa) {
           int logMonth = log.getStartTime().getMonthValue();
           float jamKerja = Math.abs((Duration.between(log.getEndTime(), log.getStartTime()).getSeconds()) / 3600);
           tempJamKerja[logMonth] += jamKerja;
       }

       for (int i = 0; i < 12; i++) {
           if (tempJamKerja[i] > 0) {
               Laporan laporan = laporanRepository.findLaporanByMahasiswaAndMonthInt(mahasiswa, i);
               System.out.println("LAPORAN: "+laporan);
               if (laporan == null) {
                   new Laporan(i, tempJamKerja[i], mahasiswa);
               } else {
                   laporan.setJamKerja(laporan.getJamKerja() + tempJamKerja[i]);
                   laporan.setPembayaran((long) (laporan.getJamKerja() * 350));
               }
           }
       }
       return laporanRepository.findLaporansByMahasiswa(mahasiswa);
    }
}
