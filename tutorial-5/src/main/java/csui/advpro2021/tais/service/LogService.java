package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;

public interface LogService {
    Log createLog(Log log);

    Iterable<Log> getLogsByMahasiswa(Mahasiswa mahasiswa);

    Log getLogById(int idLog);

    void deleteLogById(int idLog);

    Log updateLog(Log log);
}
