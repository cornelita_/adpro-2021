package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Mahasiswa;

public interface LaporanService {
   Mahasiswa createLaporan(Laporan laporan);

   Iterable<Laporan> getListLaporanByMahasiswa(String npm);

   Iterable<Laporan> updateLaporan(Mahasiswa mahasiswa);
}
