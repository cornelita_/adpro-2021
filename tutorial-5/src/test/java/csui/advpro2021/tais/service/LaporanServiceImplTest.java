package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LaporanRepository;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.sound.midi.SysexMessage;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LaporanServiceImplTest {
    @Mock
    private LaporanRepository laporanRepository;

    @InjectMocks
    private LaporanServiceImpl laporanService;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;
    private Laporan laporan;
    private Log log;
    private LocalDateTime start;

    @BeforeEach
    public void setUp() {
       mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
               "081317691718");
       start = LocalDateTime.of(2021, 4, 2, 17, 30);
       log = new Log(start, start.plusHours(1), "This is my very first log", mahasiswa);
       laporan = new Laporan(Calendar.APRIL, 0, mahasiswa);
       log.setMahasiswa(mahasiswa);
    }

    @Test
    public void testCreateLaporan() {
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        lenient().when(laporanService.createLaporan(laporan)).thenReturn(mahasiswa);
    }

    @Test
    public void testCreateLaporanNonExistMahasiswa() {
        lenient().when(laporanService.createLaporan(laporan)).thenReturn(mahasiswa);
    }

    @Test
    public void testServiceGetListLaporanNonExistMahasiswa() {
        Iterable<Laporan> listLaporanResult = laporanService.getListLaporanByMahasiswa(mahasiswa.getNpm());
        Assertions.assertIterableEquals(null, listLaporanResult);
    }

    @Test
    public void testServiceGetListLaporanByMahasiswaNoLog() {
        Iterable<Laporan> listLaporan = laporanService.updateLaporan(mahasiswa);
        lenient().when(laporanService.updateLaporan(mahasiswa)).thenReturn(listLaporan);
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Iterable<Laporan> listLaporanResult = laporanService.getListLaporanByMahasiswa(mahasiswa.getNpm());
        Assertions.assertIterableEquals(listLaporan, listLaporanResult);
    }

    @Test
    public void testServiceGetListLaporanByMahasiswa() {
        List<Log> listLog = new ArrayList<>();
        listLog.add(log);
        listLog.add(new Log(start.plusHours(1), start.plusHours(3), "This is my second log", mahasiswa));
        lenient().when(logRepository.findAllByMahasiswa(mahasiswa)).thenReturn(listLog);

        Laporan laporan2 = new Laporan(Calendar.JULY, 10, mahasiswa);
        lenient().when(laporanRepository.findLaporanByMahasiswaAndMonthInt(mahasiswa, 4)).thenReturn(laporan);
        lenient().when(laporanRepository.findLaporanByMahasiswaAndMonthInt(mahasiswa, 6)).thenReturn(laporan2);
        lenient().when(logService.getLogsByMahasiswa(mahasiswa)).thenReturn(listLog);
        Iterable<Laporan> listLaporan = laporanService.updateLaporan(mahasiswa);
        lenient().when(laporanService.updateLaporan(mahasiswa)).thenReturn(listLaporan);
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Iterable<Laporan> listLaporanResult = laporanService.getListLaporanByMahasiswa(mahasiswa.getNpm());
        Assertions.assertIterableEquals(listLaporan, listLaporanResult);
    }

    @Test
    public void testServiceGetListLaporanByMahasiswaAndLaporanEqualsNull() {
        List<Log> listLog = new ArrayList<>();
        listLog.add(log);
        listLog.add(
                new Log(start.plusMonths(2), start.plusMonths(2).plusHours(4),
                        "This is my second log", mahasiswa)
        );
        lenient().when(logRepository.findAllByMahasiswa(mahasiswa)).thenReturn(listLog);

        lenient().when(laporanRepository.findLaporanByMahasiswaAndMonthInt(mahasiswa, 4)).thenReturn(laporan);

        lenient().when(logService.getLogsByMahasiswa(mahasiswa)).thenReturn(listLog);
        Iterable<Laporan> listLaporan = laporanService.updateLaporan(mahasiswa);
        lenient().when(laporanService.updateLaporan(mahasiswa)).thenReturn(listLaporan);
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Iterable<Laporan> listLaporanResult = laporanService.getListLaporanByMahasiswa(mahasiswa.getNpm());
        Assertions.assertIterableEquals(listLaporan, listLaporanResult);
    }

    @Test
    public void testServiceUpdateLaporan() {
       Iterable<Laporan> listLaporan = laporanService.updateLaporan(mahasiswa);
       lenient().when(laporanService.updateLaporan(mahasiswa)).thenReturn(listLaporan);
    }
}
