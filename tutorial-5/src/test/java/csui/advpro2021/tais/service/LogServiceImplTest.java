package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private LogServiceImpl logService;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    private Log log;
    private Mahasiswa mahasiswa;
    private LocalDateTime start;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        start = LocalDateTime.of(2021, 4, 2, 17, 30);
        log = new Log(start, start.plusMinutes(30), "This is my very first log", mahasiswa);
    }

    @Test
    public void testServiceCreateLog() {
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        lenient().when(logService.createLog(log)).thenReturn(log);
    }

    @Test
    public void testServiceCreateLogUndefinedMahasiswa() {
        Mahasiswa mahasiswa2 = new Mahasiswa("1906192000", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        log.setMahasiswa(mahasiswa2);
        lenient().when(logService.createLog(log)).thenReturn(null);
    }

    @Test
    public void testServiceGetLogsByMahasiswa() {
        Iterable<Log> listLog = logRepository.findAllByMahasiswa(mahasiswa);
        lenient().when(logService.getLogsByMahasiswa(mahasiswa)).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getLogsByMahasiswa(mahasiswa);
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    public void testServiceGetLogById() {
        lenient().when(logService.getLogById(log.getIdLog())).thenReturn(log);
        Log logResult = logService.getLogById(log.getIdLog());
        assertEquals(log.getIdLog(), logResult.getIdLog());
    }

    @Test
    public void testServiceDeleteLog() {
        logService.createLog(log);
        logService.deleteLogById(log.getIdLog());
        lenient().when(logService.getLogById(log.getIdLog())).thenReturn(null);
        assertEquals(null, logService.getLogById(log.getIdLog()));
    }

    @Test
    public void testServiceUpdateLog() {
        lenient().when(logService.getLogById(log.getIdLog())).thenReturn(log);
        LocalDateTime currentStartTime = log.getStartTime();
        log.setStartTime(start.minusHours(1));

        lenient().when(logService.updateLog(log)).thenReturn(log);
        Log logResult = logService.updateLog(log);

        assertEquals(currentStartTime.minusHours(1), logResult.getStartTime());
    }
}
