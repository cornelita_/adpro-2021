package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LaporanServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LaporanController.class)
public class LaporanControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LaporanServiceImpl laporanService;

    private Mahasiswa mahasiswa;
    private LocalDateTime start;
    private Laporan laporan;
    private Log log;
    private DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        start = LocalDateTime.of(2021, 4, 2, 17, 30);
        log = new Log(start, start.plusHours(1), "This is my very first log", mahasiswa);
        laporan = new Laporan();
        laporan.setMonth(Month.APRIL.getDisplayName(TextStyle.FULL, Locale.getDefault()));
        laporan.setMonthInt(3);
        laporan.setJamKerja(0);
        laporan.setMahasiswa(mahasiswa);
        log.setMahasiswa(mahasiswa);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerGetLaporanByMahasiswa() throws Exception {
        Iterable<Laporan> listLaporan = Arrays.asList(laporan);
        when(laporanService.getListLaporanByMahasiswa(mahasiswa.getNpm())).thenReturn(listLaporan);

        mvc.perform(get("/laporan/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].monthInt").value(3));
    }
}
