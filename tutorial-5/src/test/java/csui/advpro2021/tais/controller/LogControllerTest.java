package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;
    private LocalDateTime start;
    private Log log;
    private DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        start = LocalDateTime.of(2021, 4, 2, 17, 30);
        log = new Log(start, start.plusMinutes(30), "This is my very first log", mahasiswa);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerCreateLog() throws Exception {
        when(logService.createLog(log)).thenReturn(log);

        mvc.perform(post("/log")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(log)))
                .andExpect(jsonPath("$.idLog").value(log.getIdLog()));
    }

    @Test
    public void testControllerGetLogsByMahasiswa() throws Exception {
        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getLogsByMahasiswa(mahasiswa)).thenReturn(listLog);

        mvc.perform(get("/log").contentType(MediaType.APPLICATION_JSON).content(mapToJson(mahasiswa)))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value(log.getIdLog()));
    }

    @Test
    public void testControllerGetLogById() throws Exception {
        when(logService.getLogById(log.getIdLog())).thenReturn(log);

        mvc.perform(get("/log/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.idLog").value(log.getIdLog()));
    }

    @Test
    public void testControllerGetNonExistLog() throws Exception {
        mvc.perform(get("/log/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerDeleteLogById() throws Exception {
        logService.createLog(log);
        mvc.perform(delete("/log/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerUpdateLog() throws Exception {
        LocalDateTime currentStartTime = log.getStartTime();
        log.setStartTime(start.minusHours(1));

        when(logService.updateLog(log)).thenReturn(log);

        mvc.perform(post("/log/update")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(log)))
                .andExpect(jsonPath("$.startTime").value(currentStartTime.minusHours(1).format(formatter)));
    }
}
