# Requirements

## Entity yang perlu dibuat

1. Mahasiswa
2. Mata Kuliah
3. Log
4. Laporan

## Keterangan

1. Mahasiswa

    a. Mahasiswa bisa mendaftar menjadi asdos
    b. Jika mahasiswa telah menjadi asdos, maka tidak dapat dihapus
    c. Mahasiswa dapat membuat dan mengupdate log
    d. Mahasiswa dapat melihat laporan pembayaran
    c. Mahasiswa dan Mata Kuliah -> many to one
    d. Mahasiswa dan Log -> one to many
    e. Mahasiswa dan Laporan -> one to many

2. Mata Kuliah

    a. Mata Kuliah yang sudah memiliki asdos, tidak dapat dihapus
    b. Mata Kuliah dan Mahasiswa -> one to many

3. Log

    a. Log dapat diupdate
    b. Log bisa kurang dari satu jam.
    c. Satu jam = 350 Greil
    d. Log dan Mahasiswa -> many to one

4. Laporan

    a. Laporan dependent terhadapa mahasiswa
    b. Laporan dan Mahasiswa -> many to one

![ERD](image/adpro-tais.jpg)
