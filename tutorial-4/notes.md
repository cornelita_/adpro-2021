# Singleton Pattern

## Lazy Instantiation

Lazy instantiation pada singleton pattern artinya instantiation hanya dilakukan ketika ada seseorang yang membutuhkan
instance dari class tersebut (pada tutorial-4 yaitu ketika memanggil method getInstance()). Dengan begitu, method
getInstance() itu akan mengecek apakah instancenya masih null. Jika masih null, maka ia akan membuatnya dan
mengembalikannya. Jika tidak null, maka ia akan langsung mengembalikannya.

### Lazy Instantiation Example (OrderDrink.java)

```
private static OrderDrink orderDrink = null;

public static OrderDrink getInstance() {
    if (orderDrink == null) {
        orderDrink = new OrderDrink();
    }
    return orderDrink;
}
```

### Keuntungan

1. Berguna untuk <em>expensive object</em> atau ketika ada kemungkinan object tersebut tidak akan digunakan
(menghemat penggunaan memory).
2. Ketika dijalankan, program akan lebih ringan karena object singleton hanya dibuat ketika dibutuhkan.

### Kerugian

1. Non Thread Safe (sulit untuk menangani kasus multithreading yaitu ketika terjadi pemanggilan method getInstance()
secara bersamaan).
2. Di method getInstance(), pengecekan apakah instance masih null akan dilakukan berkali-kali, yaitu setiap kali method
tersebut dipanggil.

## Eager Instantiation

Eager instantiation pada singleton pattern artinya instantiation akan dilakukan langsung (when the class is loaded)
tanpa menunggu apakah seseorang akan membutuhkannya atau tidak.

### Eager Instantiation Example (OrderFood.java)

```
private static OrderFood orderFood = new OrderFood();

public static OrderFood getInstance() {
    return orderFood;
}
```

### Keuntungan

1. Thread Safe artinya tidak peduli berapa banyak thread yang memanggil method getInstance(), mereka tetap akan
mengembalikan 1 object yang sama.
2. Implementasi dari eager instantiation lebih mudah.
3. Object nya siap digunakan karena udah dibuat dari awal.

### Kerugian

1. Jika object tidak pernah dibutuhkan, maka hanya akan membuang-buang memory
2. Ketika dijalankan, program akan lebih berat dibandingkan jika menggunakan lazy instantiation karena semua object
singleton akan dibuat di awal.