package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuTest {
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menu = new LiyuanSoba("Menu Test");
    }

    @Test
    public void testGetNameFromMenu() {
        assertEquals("Menu Test", menu.getName());
    }

    @Test
    public void testGetNoodleFromMenu() {
        assertTrue(menu.getNoodle() instanceof Noodle);
    }

    @Test
    public void testGetMeatFromMenu() {
        assertTrue(menu.getMeat() instanceof Meat);
    }

    @Test
    public void testGetToppingFromMenu() {
        assertTrue(menu.getTopping() instanceof Topping);
    }

    @Test
    public void testGetFlavorFromMenu() {
        assertTrue(menu.getFlavor() instanceof Flavor);
    }
}
