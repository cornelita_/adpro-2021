package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
    }

    @Test
    public void testAddShouldAddMenuToRepository() {
        Menu menu = new LiyuanSoba("Menu Test");
        menuRepository.add(menu);

        List<Menu> menuList = menuRepository.getMenus();

        assertThat(menuList).hasSize(1);
        assertThat(menuList).contains(menu);
    }
}
