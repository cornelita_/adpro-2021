package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderDrinkTest {
    @Test
    public void testOrderDrinkIsASingleton() {
        OrderDrink orderDrink = OrderDrink.getInstance();
        assertEquals(orderDrink, OrderDrink.getInstance());
    }
}
