package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;

    @BeforeEach
    public void setup() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier
                .isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testInuzamaRamenConstructor() {
        LiyuanSoba liyuanSoba1 = new LiyuanSoba("Something");
        LiyuanSoba liyuanSoba2 = new LiyuanSoba("Something");

        assertNotEquals(liyuanSoba1, liyuanSoba2);
    }
}
