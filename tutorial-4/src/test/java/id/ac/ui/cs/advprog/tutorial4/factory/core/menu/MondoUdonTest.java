package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;

    @BeforeEach
    public void setup() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier
                .isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testInuzamaRamenConstructor() {
        MondoUdon mondoUdon1 = new MondoUdon("Something");
        MondoUdon mondoUdon2 = new MondoUdon("Something");

        assertNotEquals(mondoUdon1, mondoUdon2);
    }
}
