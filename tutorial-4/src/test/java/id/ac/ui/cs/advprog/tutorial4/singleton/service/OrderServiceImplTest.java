package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class OrderServiceImplTest {
    private Class<?> orderServiceClass;

    @Mock
    private OrderDrink orderDrink;

    @Mock
    private OrderFood orderFood;

    @InjectMocks
    private OrderService orderService = new OrderServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void whenOrderADrinkIsCalledItShouldCallOrderDrinkSetDrink() throws Exception {
        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", String.class);

        assertEquals("void",
                orderADrink.getGenericReturnType().getTypeName());
        assertEquals(1,
                orderADrink.getParameterCount());
        assertTrue(Modifier.isPublic(orderADrink.getModifiers()));

        orderService.orderADrink(null);
        verify(orderDrink, atLeastOnce()).setDrink(null);
    }

    @Test
    public void whenOrderAFoodIsCalledItShouldCallOrderFoodSetFood() throws Exception {
        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", String.class);

        assertEquals("void",
                orderAFood.getGenericReturnType().getTypeName());
        assertEquals(1,
                orderAFood.getParameterCount());
        assertTrue(Modifier.isPublic(orderAFood.getModifiers()));

        orderService.orderAFood(null);
        verify(orderFood, atLeastOnce()).setFood(null);
    }

    @Test
    public void whenGetDrinkIsCalledItWillReturnOrderDrinkInstance() {
        assertTrue(orderService.getDrink() instanceof OrderDrink);
    }


    @Test
    public void whenGetFoodIsCalledItWillReturnOrderFoodInstance() {
        assertTrue(orderService.getFood() instanceof OrderFood);
    }
}
