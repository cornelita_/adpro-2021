package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier
                .isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testInuzamaRamenConstructor() {
        SnevnezhaShirataki snevnezhaShirataki1 = new SnevnezhaShirataki("Something");
        SnevnezhaShirataki snevnezhaShirataki2 = new SnevnezhaShirataki("Something");

        assertNotEquals(snevnezhaShirataki1, snevnezhaShirataki2);
    }
}
