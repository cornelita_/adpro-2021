package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoiledEggTest {
    private Class<?> boiledEggClass;
    private BoiledEgg boiledEgg;

    @BeforeEach
    public void setup() throws Exception {
        boiledEggClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
        boiledEgg = new BoiledEgg();
    }

    @Test
    public void testBoiledEggIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(boiledEggClass.getModifiers()));
    }

    @Test
    public void testBoiledEggIsATopping() {
        Collection<Type> interfaces = Arrays.asList(boiledEggClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testBoiledEggOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = boiledEggClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testBoiledEggGetDescription() {
        assertEquals("Adding Guahuan Boiled Egg Topping", boiledEgg.getDescription());
    }
}
