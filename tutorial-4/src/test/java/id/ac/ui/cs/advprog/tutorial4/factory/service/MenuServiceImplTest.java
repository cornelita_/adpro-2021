package id.ac.ui.cs.advprog.tutorial4.factory.service;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class MenuServiceImplTest {
    private Class<?> menuServiceClass;

    @Mock
    private MenuRepository menuRepository;

    @InjectMocks
    MenuService menuService = new MenuServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        ParameterizedType pt = (ParameterizedType) getMenus.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(Menu.class));
    }

    @Test
    public void whenGetMenusIsCalledItShouldCallMenuRepositoryGetMenus() {
        menuService.getMenus();
        verify(menuRepository, atLeastOnce()).getMenus();
    }
}