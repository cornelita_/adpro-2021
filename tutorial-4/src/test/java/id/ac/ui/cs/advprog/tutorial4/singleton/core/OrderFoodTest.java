package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderFoodTest {
    @Test
    public void testOrderDrinkIsASingleton() {
        OrderFood orderFood = OrderFood.getInstance();
        assertEquals(orderFood, OrderFood.getInstance());
    }
}
