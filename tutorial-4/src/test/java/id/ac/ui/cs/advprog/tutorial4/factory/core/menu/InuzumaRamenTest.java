package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier
                .isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzamaRamenConstructor() {
        InuzumaRamen inuzumaRamen1 = new InuzumaRamen("Something");
        InuzumaRamen inuzumaRamen2 = new InuzumaRamen("Something");

        assertNotEquals(inuzumaRamen1, inuzumaRamen2);
    }
}
